package org.geniusbros.spring;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	/**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
        logger.info("Welcome home! The client locale is {}.", locale);
         
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
         
        String formattedDate = dateFormat.format(date);
         
        model.addAttribute("serverTime", formattedDate );
         
        return "index";
    }
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String indexPage(Locale locale, Model model) {
        return "index";
    }
    
    @RequestMapping(value = "/acercade", method = RequestMethod.GET)
    public String aboutofPage(Locale locale, Model model) {
        return "acercade";
    }
    
    @RequestMapping(value = "/integrantes", method = RequestMethod.GET)
    public String membersPage(Locale locale, Model model) {
        return "integrantes";
    }
    
    
    @RequestMapping(value = "/proyectos", method = RequestMethod.GET)
    public String projectsPage(Locale locale, Model model) {
        return "proyectos";
    }
     
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Locale locale, Model model) {
        return "login";
    }
    
    @RequestMapping(value = "/registro", method = RequestMethod.GET)
    public String registerPage(Locale locale, Model model) {
        return "frmRegistro";
    }
     
    @RequestMapping(value = "/observaciones", method = RequestMethod.GET)
    public String notesPage(Locale locale, Model model) {
        return "observaciones";
    }
    
    @RequestMapping(value = "/perfil", method = RequestMethod.POST)
    public String login(@Validated User user, Model model) {
    	String role = user.getRole();
    	model.addAttribute("userName", user.getUserName());
    	model.addAttribute("myRole", user.getRole());
    	if(role.toLowerCase().contains("teacher") ){
    		return "profesor";
    	}
    	else if(role.toLowerCase().contains("student") ){
    		return "estudiante";
    	}
    	else if(role.toLowerCase().contains("asesor") ){
    		return "asesor";
    	}
    	else{
   		
    		return "login";
    	}
    }

	
	/**
	 * Simply selects the home view to render by returning its name.
	 
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	*/
	
}
