package org.geniusbros.spring;

import java.util.ArrayList;
import java.util.List;

public class User {

	private String userName;
	private String password;

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String userName) {
		this.password = userName;
	}

	public String getRole() {
		String role = "noone";

		if (password.equals("p123")) {
			if (userName.equalsIgnoreCase("molaya")
					|| userName.equalsIgnoreCase("anarvaez")
					|| userName.equalsIgnoreCase("yflorez")) {
				role = "Teacher";
			}
		}
		if (password.equalsIgnoreCase("e123")) {
			if (userName.equalsIgnoreCase("molaya")
					|| userName.equalsIgnoreCase("ksierra")
					|| userName.equals("emontoya")) {
				role = "Student";
			}
		}
		if (password.equalsIgnoreCase("a123")) {
			if (userName.equals("dospina")) {
				role = "Asesor";
			}
		}

		return role;
	}

}