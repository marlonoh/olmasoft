<%-- 
    Document   : acercade
    Created on : 23/06/2015, 08:07:34 PM
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Acerca de</title>
<link rel="stylesheet" href="CSS/acercade.css">
<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
</head>
<body>

	<header>

		<div class="logotipo">
			<img src="img/LogoOlma2.png" width="350" alt="">
		</div>

		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-togle="collapse"
						data-target="#acolapsar">
						<span class="sr-only">Ventana de navegacion</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>

				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index"><span
								class="glyphicon glyphicon-home"></span> Inicio</a></li>
						<li><a href="acercade"><span
								class="glyphicon glyphicon-home"></span> Acerca de</a></li>
						<li><a href="proyectos"><span
								class="glyphicon glyphicon-list-alt"></span> Proyectos</a></li>

						<li><a href="integrantes"><span
								class="glyphicon glyphicon-education"></span> Integrantes</a></li>
						<li><a href="observaciones"><span
								class="glyphicon glyphicon-file"></span> Observaciones</a></li>

						</li>
					</ul>
				</div>
			</div>
		</nav>





	</header>


	<section class="main">
		<section class="articles">


			<article>
				<h2>Descripción del problema</h2> <br/>
				<p>Los asesores de proyectos del Politécnico Jaime Isaza Cadavid, ayudan a los jóvenes de los grados 10 y 11 a realizar  un proyecto de programación. Suelen tener problemas con la forma en la que se guardan los proyectos de los jóvenes de todas las instituciones.

				En ocasiones los asesores y/o profesores se les dificulta encontrar información sobre algunos proyectos, ya que el lugar  donde guardan la información no es muy amigable a la hora de diligenciarla y no es muy entendible para algunos asesores.
				</p>
			</article>	


			<article>
				<h2>Alcance</h2> <br/>
				<p>El software permitirá el manejo de las instituciones educativas, los nombres de los asesores o profesores, nombre del proyecto, nombres de los integrantes de los equipos y un lugar en el cual ellos puedan escribir las observaciones que se le hacen a cada uno de los proyectos.
				Además permitirá un mejoramiento en el orden de los datos de los equipos y la manera en la que se guarda la información de estos.
				</p>
			</article>	


			<article>
				<h2>Objetivo general</h2> <br/>
				<p>Desarrollar un software que permita el control y el manejo de la información del proyecto pedagógico integrador (PPI)</p>
			</article>	


			<article>
				<h2>Objetivos Especificos</h2> <br/>
				<p>•	Analizar las diferentes necesidades de los asesores del PPI (Proyecto pedagógico integrado) mediante preguntas detalladas, moldeando los diagramas de caso de uso con los requisitos funcionales y de usuario. </p>

				<p>•	Diseñar las diferentes interfaces gráficas que compone el software.</p>

				<p>•	Codificar los diferentes módulos enlazándolos a la base de datos.</p>

				<p>•	Testear la aplicación antes de  entregarla al cliente realizando las mejoras necesarias.</p>
				<p>•	Implementar la aplicación funcionando al cliente.</p>
				
			</article>
		</section>

		
	</section>

	<footer>
		Kevin Sierra, Esteban Montoya, Mateo Olaya- Copyright © Todos los derechos reservados. Olma's Soft (2014 - 2015).   	
		
	</footer>







	<script src="js/jquery-1.11.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	
</body>
</html>