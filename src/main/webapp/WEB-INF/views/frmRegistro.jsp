<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Registrate</title>
	<link rel="stylesheet" href="CSS/frmRegistro.css">
	<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Amatic+SC' rel='stylesheet' type='text/css'>
</head>
<body>
	<div class="container">
		<img src="img/LogoOlma2.png" alt="" width="350" class="logo">
		
		
			<section class="main">
				<section class="conditions">
					<article>
						<h2>Terminos y condiciones</h2>
						<p>
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
						</p>
					</article>
				</section>
			</section>
				<form role="form" name="form1">
					
					<div class="form-group">
						 <span></span>
						 <label for="txtNombre" id="user">Nombre Completo:</label>
      					<input type="text" class="form-control" id="txtNombre" placeholder="Ej: Ruben Doblas Gundersen" name="txtNombre" required>
					</div>
					<div class="form-group">
						<label for="txtUser">Nombre de Usuario: </label>
      					<input type="text" class="form-control" id="txtUser" placeholder="Elige tu nombre de usuario" name="lblUser"required>
					</div>
					<div class="form-group">
						<label for="txtPwd">Contraseņa: </label>
      					<input type="password" class="form-control" id="txtPwd" placeholder="Elige una contraseņa." name="txtPwd" required>
					</div>
					
					<div class="form-group">
						<label for="txtConffirmPwd">Confirmar contraseņa: </label>
      					<input type="password" class="form-control" id="txtConffirmPwd" placeholder="Confirma tu contraseņa." name="txtConffirmPwd" required>
					</div>

					<div class="form-group">
						<label for="txtEmail">Correo: </label>
      					<input type="email" class="form-control" id="txtEmail" placeholder="Confirma tu contraseņa." name="txtEmail" required>
					</div>


					<input type="button" value="Registrate" id="enviar" onClick="confirmacionPwd()">

				</form>
			</div>

			<script type="text/javascript" src="js/confirmacionPwd.js"></script>
	
</body>
</html>