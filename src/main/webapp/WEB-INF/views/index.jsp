<%-- 
    Document   : index
    Created on : 23/06/2015, 06:58:40 PM
    Author     : Administrador


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PruebaJsp</title>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>

    Document   : index
    Created on : 23/06/2015, 06:58:40 PM
    Author     : Administrador
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PruebaJsp</title>
<link rel="stylesheet" href="CSS/style.css">
<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
</head>
<body>

	<header>

		<div class="logotipo">
			<img
				src="${pageContext.request.contextPath}/resources/img/LogoOlma2.png"
				width="350" alt="">
		</div>

		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-togle="collapse"
						data-target="#acolapsar">
						<span class="sr-only">Ventana de navegacion</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>

				</div>

				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index"><span
								class="glyphicon glyphicon-home"></span> Inicio</a></li>
						<li><a href="acercade"><span
								class="glyphicon glyphicon-comment"></span> Acerca de</a></li>
						<li><a href="proyectos"><span
								class="glyphicon glyphicon-list-alt"></span> Proyectos</a></li>

						<li><a href="integrantes"><span
								class="glyphicon glyphicon-education"></span> Integrantes</a></li>
						<li><a href="observaciones"><span
								class="glyphicon glyphicon-file"></span> Observaciones</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="login"><span
								class="glyphicon glyphicon-user"></span> Inciar Sesión</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<section class="main">
		<section class="articles">
			<article>
				<h2>Misión</h2>
				<p>Generar, desarrollar, asimilar y aplicar los conocimientos
					adquiridos en las clases institucionales para promover la
					organización de los proyectos del PPI (Proyecto Pedagógico
					Integrado) y contribuir al desarrollo organizado de estos.</p>
			</article>

			<article>
				<h2>Visión</h2>
				<p>En el año 2016, Olma´s Soft, se destacará por la organización
					y al desarrollo de los proyectos PPI (Proyecto Pedagógico
					Integrado) transformando el manejo actual de estas actividades en
					una forma más ágil de buscar, agregar, eliminar y editar proyectos.</p>
			</article>
		</section>
	</section>

	<footer> Kevin Sierra, Esteban Montoya, Mateo Olaya- Copyright
		© Todos los derechos reservados. Olma's Soft (2014 - 2015). </footer>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
